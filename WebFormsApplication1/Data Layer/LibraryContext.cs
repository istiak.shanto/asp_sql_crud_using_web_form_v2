﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using WebFormsApplication1.Model;

namespace WebFormsApplication1.Data_Layer
{
    public class LibraryContext: DbContext
    {

        public LibraryContext():base("LibraryDB") 
        {

        }

        public DbSet<Author> Authors { get; set; }
        public DbSet<Book> Books { get; set; }

    }
}