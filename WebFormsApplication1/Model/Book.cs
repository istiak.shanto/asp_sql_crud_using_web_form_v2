﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebFormsApplication1.Model
{
    public class Book
    {

        public int BookId { get; set; }
        public string Title { get; set; }

        public string Description { get; set; }

        public string AuthorId { get; set; }

        public virtual  Author Author { get; set; }
    }
}